import matplotlib

matplotlib.use('TKAgg')

import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib.widgets import TextBox, Button, RadioButtons, CheckButtons

WIDTH = 100
HEIGHT = 100
tmpWIDTH = WIDTH
tmpHEIGHT = HEIGHT
rulenum = 90
random_start = False
vals = (1, 0)


def gen():
    n = HEIGHT
    i = 0
    while i < n:
        yield i
        i += 1


def init(randrow=False):
    grid = np.zeros(shape=(HEIGHT, WIDTH))
    if randrow:
        grid[0] = np.random.choice(vals, WIDTH, p=[0.2, 0.8])
    else:
        grid[0][WIDTH // 2] = 1
    return grid


def create_rule(rulenum):
    rule_bin = '{:08b}'.format(rulenum)
    rule = [int(x) for x in rule_bin][::-1]
    print(rule_bin)
    return rule


def update(d):
    # print(d)
    data = grid[d]
    if d + 1 >= HEIGHT:
        return
    grid[d + 1] = np.array(
        [rule[
             int(4 * data[(j - 1) % WIDTH] + 2 * data[j] + data[(j + 1) % WIDTH])
         ] for j in range(WIDTH)]
    )

    mat.set_data(grid)
    #return mat


def plot_init():
    global grid, rule, a, WIDTH, HEIGHT, fig
    WIDTH = tmpWIDTH
    HEIGHT = tmpHEIGHT
    ax.set_aspect(HEIGHT / WIDTH, adjustable='box')
    # fig.set_figheight(square_h * HEIGHT)
    # fig.set_figwidth(square_w * WIDTH)
    grid = init(randrow=random_start)
    rule = create_rule(rulenum)
    a = gen()


def start_animation():
    global ani
    print("start")
    ani = animation.FuncAnimation(fig, update, frames=gen, interval=60, repeat=False)


fig, ax = plt.subplots()
square_w = fig.get_figwidth() / WIDTH
square_h = fig.get_figheight() / HEIGHT
ax.axis('off')
grid = None
rule = None
a = None
plot_init()
mat = ax.matshow(grid, cmap=cm.Greens, aspect='equal')

rax = plt.axes([0.025, 0.5, 0.15, 0.15])
radio = RadioButtons(rax, [30, 60, 90, 120, 225], active=2)

resetax = plt.axes([0.85, 0.025, 0.1, 0.04])
button = Button(resetax, 'Reset', hovercolor='0.975')

textax1 = plt.axes([0.85, 0.85, 0.1, 0.04])
textbox1 = TextBox(textax1, 'W:', initial=str(WIDTH))

textax2 = plt.axes([0.85, 0.75, 0.1, 0.04])
textbox2 = TextBox(textax2, 'H:', initial=str(HEIGHT))


def radiofunc(label):
    global rulenum
    rulenum = int(label)


radio.on_clicked(radiofunc)


def reset(event):
    global textbox1, textbox2
    plot_init()
    textbox1.set_val(str(WIDTH))
    textbox2.set_val(str(HEIGHT))
    start_animation()


button.on_clicked(reset)


def set_width(event):
    global tmpWIDTH
    try:
        tmpWIDTH = int(event)
    except:
        pass


textbox1.on_submit(set_width)


def set_height(event):
    global tmpHEIGHT
    try:
        tmpHEIGHT = int(event)
    except:
        pass


textbox2.on_submit(set_height)

ani = None
start_animation()
plt.show()
