import matplotlib.pyplot as plt
from matplotlib import use as matplotlib_use
import matplotlib
import numpy as np

matplotlib_use('TKAgg')
i = 1
grid = np.zeros(shape=(10, 10))

# tab20=matplotlib.cm.get_cmap('tab20',20)
# white = np.array([1.0,1.0,1.0,1.0])
# newcolors = tab20(np.linspace(0,1,20))
# newcolors[:20,:] = white
# newcmp = matplotlib.colors.ListedColormap(newcolors)


def onclick(event):
    global i
    if event.button == 1:
        if event.inaxes == ax:
            grid[int(round(event.ydata))][int(round(event.xdata))] = i
            i += 1
    # clear frame
    print(grid)
    mat.set_data(grid)
    mat.set_norm(matplotlib.colors.Normalize(vmin=0.0000000001))
    plt.draw()  # redraw

print(matplotlib.cm.tab20)
fig, ax = plt.subplots()
cmap = plt.cm.hsv
cmap.set_under(color='white')
mat = ax.matshow(grid, cmap=cmap, norm=matplotlib.colors.Normalize(vmin=0.0000000001, vmax = 1), aspect='equal')
fig.canvas.mpl_connect('button_press_event', onclick)
plt.show()
plt.draw()
