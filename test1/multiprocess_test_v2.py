import concurrent.futures
import math
import numpy as np

PRIMES = None
def is_prime(n):
    if n % 2 == 0:
        return False

    sqrt_n = int(math.floor(math.sqrt(n)))
    for i in range(3, sqrt_n + 1, 2):
        if n % i == 0:
            return False
    return True


def printt(string):
    string += 1
    return string


def main():
    with concurrent.futures.ProcessPoolExecutor() as executor:
        b = executor.map(printt, [a[x, :] for x in range(len(a))])
    b = np.asarray(list(b))
        # for number, prime in zip(range(len(a)), executor.map(printt, [a[x,:] for x in range(len(a))])):
        #     print('%d is prime: %s' % (number, prime))
        #     pass
    return b

if __name__ == '__main__':
    a = np.arange(100).reshape((10,10))
    print(a)
    a = main()

    print(a)
