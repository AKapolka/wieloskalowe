from matplotlib import use as matplotlib_use

matplotlib_use('TKAgg')

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import mpl_toolkits.axes_grid1
import matplotlib.widgets
import matplotlib.cm as cm
import unidecode

WIDTH = 100
HEIGHT = 100
tmpWIDTH = WIDTH
tmpHEIGHT = HEIGHT
random_start = True
vals = (1, 0)
option = 'Losowe'
ani=None

class SwitchBase:
    def switch(self, case):
        m = getattr(self, 'case_{}'.format(unidecode.unidecode(case).lower()), None)
        if not m:
            return self.default()
        return m()

    __call__ = switch


class CustomSwitcher(SwitchBase):
    def case_niezmienny(self):
        global grid
        grid = init(randrow=False)
        x1 = np.random.randint(0, WIDTH)
        y1 = np.random.randint(0, HEIGHT)
        pairs = offset(neighboring_cells_beehive, (x1, y1))
        for x, y in pairs:
            grid[y][x] = 1

    def case_oscylator(self):
        global grid
        grid = init(randrow=False)
        x1 = np.random.randint(0, WIDTH)
        y1 = np.random.randint(0, HEIGHT)
        pairs = offset(neighboring_cells_blinker, (x1, y1))
        for x, y in pairs:
            grid[y][x] = 1

    def case_glider(self):
        global grid
        grid = init(randrow=False)
        x1 = np.random.randint(0, WIDTH)
        y1 = np.random.randint(0, HEIGHT)
        pairs = offset(neighboring_cells_glider, (x1, y1))
        for x, y in pairs:
            grid[y][x] = 1

    def case_losowe(self):
        global grid
        grid = init(randrow=True)

    def case_reczne(self):
        global grid
        grid = init(randrow=False)

    def default(self):
        raise Exception('Not a case!')


switch_option = CustomSwitcher()


class Player(FuncAnimation):
    def __init__(self, fig, func, frames=None, init_func=None, fargs=None,
                 save_count=None, mini=0, maxi=100, pos=(0.125, 0.92), **kwargs):
        self.init_i = -1
        self.init_min = mini
        self.init_max = maxi
        self.init_runs = True
        self.init_forwards = True
        self.init_fig = fig
        self.init_func = func
        self.init_pos = pos
        resetax = plt.axes([0.85, 0.025, 0.1, 0.04])
        self.button_reset = matplotlib.widgets.Button(resetax, 'Reset', hovercolor='0.975')
        self.button_reset.on_clicked(self.reset)
        # self.setup(pos)
        self.post_init(self.init_fig, self.init_func, pos=self.init_pos, mini=self.init_min, maxi=self.init_max)

    def post_init(self, fig, func, frames=None, init_func=None, fargs=None,
                  save_count=None, mini=0, maxi=100, pos=(0.125, 0.92), **kwargs):
        self.i = self.init_i
        self.runs = self.init_runs
        self.forwards = self.init_forwards
        self.fig = self.init_fig
        self.func = self.init_func
        plot_init()
        self.min = self.init_min
        self.max = 99
        self.setup(pos)
        print('preani')
        FuncAnimation.__init__(self, self.fig, self.func, frames=self.play(), interval=60,
                               init_func=init_func, fargs=fargs,
                               save_count=save_count, **kwargs, )

    def play(self):
        while self.runs:
            self.i = self.i + self.forwards - (not self.forwards)
            if self.i >= self.min and self.i < self.max:
                yield self.i
            else:
                self.stop()
                yield self.i

    def start(self):
        self.runs = True
        self.event_source.start()

    def stop(self, event=None):
        self.runs = False
        self.event_source.stop()

    def forward(self, event=None):
        self.forwards = True
        self.start()

    def backward(self, event=None):
        self.forwards = False
        self.start()

    def oneforward(self, event=None):
        self.forwards = True
        self.onestep()

    def onebackward(self, event=None):
        self.forwards = False
        self.onestep()

    def onestep(self):
        if self.i > self.min and self.i < self.max:
            self.i = self.i + self.forwards - (not self.forwards)
        elif self.i == self.min and self.forwards:
            self.i += 1
        elif self.i == self.max and not self.forwards:
            self.i -= 1
        self.func(self.i)
        self.fig.canvas.draw_idle()

    def reset(self, event):
        # self.post_init(self.init_fig, self.init_func, pos=self.init_pos, mini=self.init_min, maxi=self.init_max)
        self.stop()
        self.i = 0
        plot_init()
        self.fig.canvas.draw_idle()
        # plot_init()
        # textbox1.set_val(str(WIDTH))
        # textbox2.set_val(str(HEIGHT))
        # start_animation()

    def radiofunc(self, label):
        global option
        option = label
        print(label)

    def on_click(self, event):
        # Check where the click happened
        if not self.runs and self.fig.contains(event):
            if event.inaxes == ax:
                print(event.xdata,event.ydata)
                # print(int(round(event.xdata)),int(round(event.ydata)))
                # print(grid[int(round(event.ydata))][int(round(event.xdata))], end=' , ')
                if grid[int(round(event.ydata))][int(round(event.xdata))]:
                    grid[int(round(event.ydata))][int(round(event.xdata))] = 0
                else:
                    grid[int(round(event.ydata))][int(round(event.xdata))] = 1
                # print(grid[int(round(event.ydata))][int(round(event.xdata))])
                # print(grid)
                mat.set_data(grid)
                self.fig.canvas.draw_idle()
                # plt.show()
            # Event happened within the slider, ignore since it is handled in update_slider
            return
        else:
            # user clicked somewhere else on canvas = unpause
            # global is_manual
            # is_manual = False
            pass



    def setup(self, pos):
        playerax = self.fig.add_axes([pos[0], pos[1], 0.22, 0.04])
        divider = mpl_toolkits.axes_grid1.make_axes_locatable(playerax)
        bax = divider.append_axes("right", size="80%", pad=0.05)
        sax = divider.append_axes("right", size="80%", pad=0.05)
        fax = divider.append_axes("right", size="80%", pad=0.05)
        ofax = divider.append_axes("right", size="100%", pad=0.05)
        rax = plt.axes([0.025, 0.5, 0.15, 0.15])
        labels = ['Niezmienny', 'Oscylator', 'Glider', 'Losowe', 'Ręczne']

        self.button_oneback = matplotlib.widgets.Button(playerax, label=u'$\u29CF$')
        self.button_back = matplotlib.widgets.Button(bax, label=u'$\u25C0$')
        self.button_stop = matplotlib.widgets.Button(sax, label=u'$\u25A0$')
        self.button_forward = matplotlib.widgets.Button(fax, label=u'$\u25B6$')
        self.button_oneforward = matplotlib.widgets.Button(ofax, label=u'$\u29D0$')
        self.radio = matplotlib.widgets.RadioButtons(rax, labels, active=labels.index(option))

        self.button_oneback.on_clicked(self.onebackward)
        self.button_back.on_clicked(self.backward)
        self.button_stop.on_clicked(self.stop)
        self.button_forward.on_clicked(self.forward)
        self.button_oneforward.on_clicked(self.oneforward)
        self.radio.on_clicked(self.radiofunc)
        self.fig.canvas.mpl_connect('button_press_event', self.on_click)


### using this class is as easy as using FuncAnimation:

def init(randrow=False):
    grid = np.zeros(shape=(HEIGHT, WIDTH))
    if randrow:
        for x in range(HEIGHT):
            grid[x] = np.random.choice(vals, WIDTH, p=[0.2, 0.8])
    return grid


def plot_init():
    global grid, rule, WIDTH, HEIGHT, fig, mat
    WIDTH = tmpWIDTH
    HEIGHT = tmpHEIGHT
    ax.set_aspect(HEIGHT / WIDTH, adjustable='box')
    # grid = init(randrow=random_start)
    switch_option(option)

    mat = ax.matshow(grid, cmap=cm.Reds, aspect='equal', vmin = 0, vmax = 1)


neighboring_cells_moore = [(-1, -1), (0, -1), (1, -1),
                           (-1, 0), (1, 0),
                           (-1, 1), (0, 1), (1, 1)]

neighboring_cells_neumann = [(0, -1),
                             (-1, 0), (1, 0),
                             (0, 1)]

neighboring_cells_glider = [(1, 0), (2, 0),
                            (0, 1), (1, 1),
                            (2, 2)]

neighboring_cells_blinker = [(1, 0),
                             (1, 1),
                             (1, 2)]

neighboring_cells_beehive = [(1, 0), (2, 0),
                             (0, 1), (3, 1),
                             (1, 2), (2, 2)]


def offset(cells, delta):
    "Slide/offset all the cells by delta, a (dx, dy) vector."
    (dx, dy) = delta
    return [((x + dx) % WIDTH, (y + dy) % HEIGHT) for (x, y) in cells]


fig, ax = plt.subplots()
square_w = fig.get_figwidth() / WIDTH
square_h = fig.get_figheight() / HEIGHT
ax.axis('off')
grid = None
a = None
mat = None

textax1 = plt.axes([0.85, 0.85, 0.1, 0.04])
textbox1 = matplotlib.widgets.TextBox(textax1, 'W:', initial=str(WIDTH))

textax2 = plt.axes([0.85, 0.75, 0.1, 0.04])
textbox2 = matplotlib.widgets.TextBox(textax2, 'H:', initial=str(HEIGHT))


def set_width(event):
    global tmpWIDTH
    try:
        tmpWIDTH = int(event)
    except:
        pass


textbox1.on_submit(set_width)


def set_height(event):
    global tmpHEIGHT
    try:
        tmpHEIGHT = int(event)
    except:
        pass


textbox2.on_submit(set_height)


def update(d):
    print(d)
    global grid

    neighbors = np.zeros_like(grid)
    grid_prev = np.copy(grid)
    it = np.nditer(grid_prev, flags=['multi_index'], op_flags=['writeonly'])
    # print(grid)
    while not it.finished:
        # y1 = it.multi_index[0]
        # x1 = it.multi_index[1]
        gg = offset(neighboring_cells_moore, (it.multi_index[1], it.multi_index[0]))
        a = [grid_prev[y % HEIGHT][x % WIDTH] for (x, y) in gg]
        neighbors[it.multi_index[0]][it.multi_index[1]] = sum(a)
        if it[0]:
            if neighbors[it.multi_index[0]][it.multi_index[1]] < 2 or neighbors[it.multi_index[0]][
                it.multi_index[1]] > 3:
                grid[it.multi_index[0]][it.multi_index[1]] = 0
        else:
            if neighbors[it.multi_index[0]][it.multi_index[1]] == 3:
                grid[it.multi_index[0]][it.multi_index[1]] = 1

        it.iternext()

    mat.set_data(grid)
    # return mat



ani = Player(fig, update, maxi=HEIGHT)

plt.show()
