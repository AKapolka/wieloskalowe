import math

from matplotlib import use as matplotlib_use
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import mpl_toolkits.axes_grid1
import matplotlib.widgets
import matplotlib.cm as cm
import unidecode
from zaj3.kdtree import *
import time
from itertools import product

current_milli_time = lambda: int(round(time.time() * 1000))
matplotlib_use('TKAgg')

WIDTH = 100
HEIGHT = 100
tmpWIDTH = WIDTH
tmpHEIGHT = HEIGHT
random_start = True
vals = (1, 0)
option = 'Jednorodne'
bc = 'Periodyczne'
neighbor_name = 'Von Neumann'
bc_tmp = bc
neighbor_name_tmp = neighbor_name
cell_id = 1
r = 1
kt = 3
MC_count = int(HEIGHT * WIDTH / 100)
ani = None
changed = False
show_energy = False

class SwitchBase:
    def switch(self, case):
        m = getattr(self, 'case_{}'.format(unidecode.unidecode(case).lower().translate(str.maketrans(' ', '_'))), None)
        if not m:
            return self.default()
        return m()

    __call__ = switch


class Option_Switch(SwitchBase):
    def case_jednorodne(self):
        global grid
        grid = init(random=False)
        dx = int(WIDTH // cell_id)
        dy = int(HEIGHT // r)
        x = list(range(int(dx // 2), WIDTH, dx))
        y = list(range(int(dy // 2), HEIGHT, dy))
        i = 1
        for (x, y) in product(x, y):
            grid[y][x] = i
            i += 1

    def case_z_promieniem(self):
        global grid
        grid = init(with_r=True)

    def case_losowe(self):
        global grid
        grid = init(random=True)

    def case_reczne(self):
        global grid
        grid = init(random=False)

    def default(self):
        raise Exception('Not a case!')


class Neighbor_Template_Switch(SwitchBase):
    neighboring_cells_moore = np.array([[1, 1, 1],
                                        [1, 0, 1],
                                        [1, 1, 1]])

    neighboring_cells_neumann = np.array([[0, 1, 0],
                                          [1, 0, 1],
                                          [0, 1, 0]])

    neighboring_cells_pent = np.array([[0, 0, 0],
                                       [1, 0, 1],
                                       [1, 1, 1]])

    # arr = np.rot90(neighboring_cells_pent,np.random.randint(4))
    # (a1,b1) = np.nonzero(arr)
    # for(x,y) in zip(b1,a1):
    # print(x,y)

    neighboring_cells_hex = np.array([[1, 1, 0],
                                      [1, 0, 1],
                                      [0, 1, 1]])

    # ['Von Neumann', 'Moore', 'Pentagonalne', 'Heksagonalne', 'Z promieniem']
    def case_von_neumann(self):
        (y_arr, x_arr) = np.nonzero(self.neighboring_cells_neumann)
        return list(zip(x_arr - 1, y_arr - 1))

    def case_moore(self):
        (y_arr, x_arr) = np.nonzero(self.neighboring_cells_moore)
        return list(zip(x_arr - 1, y_arr - 1))

    def case_pentagonalne(self):
        arr = np.rot90(self.neighboring_cells_pent, np.random.randint(4))
        (y_arr, x_arr) = np.nonzero(arr)
        return list(zip(x_arr - 1, y_arr - 1))

    def case_heksagonalne(self):
        arr = np.rot90(self.neighboring_cells_hex, np.random.randint(2))
        (y_arr, x_arr) = np.nonzero(arr)
        return list(zip(x_arr - 1, y_arr - 1))

    def case_hex_prawe(self):
        (y_arr, x_arr) = np.nonzero(self.neighboring_cells_hex)
        return list(zip(x_arr - 1, y_arr - 1))

    def case_hex_lewe(self):
        arr = np.rot90(self.neighboring_cells_hex, 1)
        (y_arr, x_arr) = np.nonzero(arr)
        return list(zip(x_arr - 1, y_arr - 1))


switch_option = Option_Switch()
switch_neighbor = Neighbor_Template_Switch()


class Player(FuncAnimation):
    def __init__(self, fig, func, frames=None, init_func=None, fargs=None,
                 save_count=None, mini=0, maxi=100, pos=(0.125, 0.92), **kwargs):
        self.init_i = 0
        self.init_min = mini
        self.init_max = maxi
        self.init_runs = True
        self.init_forwards = True
        self.init_fig = fig
        self.init_func = func
        self.init_pos = pos
        resetax = plt.axes([0.85, 0.025, 0.1, 0.04])
        self.button_reset = matplotlib.widgets.Button(resetax, 'Reset', hovercolor='0.975')
        self.button_reset.on_clicked(self.reset)
        # self.setup(pos)
        self.post_init(self.init_fig, self.init_func, pos=self.init_pos, mini=self.init_min, maxi=self.init_max)

    def post_init(self, fig, func, frames=None, init_func=None, fargs=None,
                  save_count=None, mini=0, maxi=100, pos=(0.125, 0.92), **kwargs):
        self.i = self.init_i
        self.runs = self.init_runs
        self.forwards = self.init_forwards
        self.fig = self.init_fig
        self.func = self.init_func
        plot_init()
        self.min = self.init_min
        self.max = maxi
        self.setup(pos)
        print('preani')
        FuncAnimation.__init__(self, self.fig, self.func, frames=self.play(), interval=60,
                               init_func=init_func, fargs=fargs,
                               save_count=save_count, **kwargs, )

    def play(self):
        while self.runs:
            self.i = self.i + self.forwards - (not self.forwards)
            if self.i < self.max:
                yield self.i
            else:
                self.stop()
                yield self.i

    def start(self):
        self.runs = True
        self.event_source.start()

    def stop(self, event=None):
        self.runs = False
        self.event_source.stop()

    def forward(self, event=None):
        self.forwards = True
        self.start()

    def backward(self, event=None):
        # self.forwards = False
        self.start()

    def oneforward(self, event=None):
        self.forwards = True
        self.onestep()

    def onebackward(self, event=None):
        # self.forwards = False
        self.onestep()

    def onestep(self):
        if self.i > self.min and self.i < self.max:
            self.i = self.i + self.forwards - (not self.forwards)
        elif self.forwards:
            self.i += 1
        elif self.i == self.max and not self.forwards:
            self.i -= 1
        self.func(self.i)
        self.fig.canvas.draw_idle()

    def reset(self, event=None):
        self.stop()
        self.i = 0
        texterr.set_visible(False)
        plot_init()
        self.max = int(HEIGHT * 1.44)
        self.fig.canvas.draw_idle()

    def radiofunc(self, label):
        global option
        option = label
        print(label)

    def radiofunc_bc(self, label):
        global bc_tmp
        bc_tmp = label
        print(label)

    def radiofunc_neigh(self, label):
        global neighbor_name_tmp
        neighbor_name_tmp = label
        print(label)

    def on_click(self, event):
        if not self.runs and self.fig.contains(event):
            if event.inaxes == ax:
                if grid[int(round(event.ydata))][int(round(event.xdata))]:
                    grid[int(round(event.ydata))][int(round(event.xdata))] = 0
                else:
                    grid[int(round(event.ydata))][int(round(event.xdata))] = cell_id
                mat.set_data(grid)
                mat.set_norm(matplotlib.colors.Normalize(vmin=0.0000000001))
                self.fig.canvas.draw_idle()

    def setup(self, pos):
        playerax = self.fig.add_axes([pos[0], pos[1], 0.22, 0.04])
        divider = mpl_toolkits.axes_grid1.make_axes_locatable(playerax)
        bax = divider.append_axes("right", size="80%", pad=0.05)
        sax = divider.append_axes("right", size="80%", pad=0.05)
        fax = divider.append_axes("right", size="80%", pad=0.05)
        ofax = divider.append_axes("right", size="100%", pad=0.05)
        rax = plt.axes([0.025, 0.5, 0.15, 0.15], frameon=False)
        rax_bc = plt.axes([0.025, 0.7, 0.15, 0.15], frameon=False)
        rax_neigh = plt.axes([0.025, 0.3, 0.15, 0.2], frameon=False)
        mcax = plt.axes([0.75, 0.025, 0.1, 0.04])
        energyswitchax = plt.axes([0.85, 0.2, 0.1, 0.04])
        labels = ['Jednorodne', 'Z promieniem', 'Losowe', 'Ręczne']
        labels_bc = ['Periodyczne', 'Absorbujące']
        labels_neigh = ['Von Neumann', 'Moore', 'Pentagonalne', 'Heksagonalne', 'Hex Prawe', 'Hex Lewe', 'Z promieniem']

        self.button_oneback = matplotlib.widgets.Button(playerax, label=u'$\u29CF$')
        self.button_back = matplotlib.widgets.Button(bax, label=u'$\u25C0$')
        self.button_stop = matplotlib.widgets.Button(sax, label=u'$\u25A0$')
        self.button_forward = matplotlib.widgets.Button(fax, label=u'$\u25B6$')
        self.button_oneforward = matplotlib.widgets.Button(ofax, label=u'$\u29D0$')
        self.radio = matplotlib.widgets.RadioButtons(rax, labels, active=labels.index(option))
        self.radio_bc = matplotlib.widgets.RadioButtons(rax_bc, labels_bc, active=labels_bc.index(bc))
        self.radio_neigh = matplotlib.widgets.RadioButtons(rax_neigh, labels_neigh,
                                                           active=labels_neigh.index(neighbor_name))
        self.button_mc = matplotlib.widgets.Button(mcax, label='MC Iter')
        self.button_energyswitch = matplotlib.widgets.Button(energyswitchax, label='Energy')

        # def resize_buttons(r, f):
        #     "Resize all radio buttons in `r` collection by fractions `f`"
        #     [c.set_radius(c.get_radius() * f) for c in r.circles]

        # resize_buttons(self.radio, 0.5)
        self.button_oneback.on_clicked(self.onebackward)
        self.button_back.on_clicked(self.backward)
        self.button_stop.on_clicked(self.stop)
        self.button_forward.on_clicked(self.forward)
        self.button_oneforward.on_clicked(self.oneforward)
        self.radio.on_clicked(self.radiofunc)
        self.radio_bc.on_clicked(self.radiofunc_bc)
        self.radio_neigh.on_clicked(self.radiofunc_neigh)
        self.button_mc.on_clicked(update_energy)
        self.button_energyswitch.on_clicked(switch_view)

        self.fig.canvas.mpl_connect('button_press_event', self.on_click)


### using this class is as easy as using FuncAnimation:

def init(random=False, with_r=False):
    def check_dist(pairs, dist):
        P = lambda *coords: list(coords)
        kd1 = KdTree([P(x, y) for (x, y) in pairs],
                     Orthotope(P(0, 0), P(WIDTH, HEIGHT)))
        for (x, y) in deepcopy(pairs):
            n = find_nearest(2, kd1, P(x, y))
            print('[{}, {}]'.format(x, y))
            # print(n.nearest, n.dist_sqd)
            if n.dist_sqd < dist:
                pairs.discard((x, y))
                print('DISCARDED')
        return pairs

    grid = np.zeros(shape=(HEIGHT, WIDTH))
    if with_r:
        dist = r * r
        x = np.random.randint(0, WIDTH, cell_id)
        y = np.random.randint(0, HEIGHT, cell_id)
        pairs = set(zip(x, y))
        pairs = check_dist(pairs, dist)
        t0 = current_milli_time()
        if len(pairs) != cell_id:
            texterr.set_visible(True)
            while current_milli_time() - t0 < 3000:
                x1 = np.random.randint(0, WIDTH)
                y1 = np.random.randint(0, HEIGHT)
                pairs.add((x1, y1))
                pairs = check_dist(pairs, dist)
                if len(pairs) == cell_id:
                    texterr.set_visible(False)
                    break

        i = 1
        for (x, y) in pairs:
            print(x, y)
            grid[y][x] = i
            i += 1
    elif random:
        x = np.random.randint(0, WIDTH, cell_id)
        y = np.random.randint(0, HEIGHT, cell_id)
        pairs = set(zip(x, y))
        while len(pairs) != cell_id:
            x1 = np.random.randint(0, WIDTH)
            y1 = np.random.randint(0, HEIGHT)
            pairs.add((x1, y1))

        i = 1
        for (x, y) in pairs:
            grid[y][x] = i
            i += 1
    return grid


def create_center_of_gravity():
    global centre_of_mass, centre_tree
    centre_of_mass = np.zeros_like(grid)
    centre_of_mass = np.stack(
        (np.random.random_sample(centre_of_mass.shape), np.random.random_sample(centre_of_mass.shape)))
    pairs = zip([x for x in np.nditer(centre_of_mass[0])], [y for y in np.nditer(centre_of_mass[1])])
    P = lambda *coords: list(coords)
    # for (x, y) in pairs:
    #     print(x,y)
    centre_tree = KdTree([P(x, y) for (x, y) in pairs],
                         Orthotope(P(0, 0), P(WIDTH, HEIGHT)))


def plot_init():
    global grid, WIDTH, HEIGHT, fig, mat, bc, neighbor_name, neighbor_cells
    WIDTH = tmpWIDTH
    HEIGHT = tmpHEIGHT
    bc = bc_tmp
    neighbor_name = neighbor_name_tmp
    # neighbor_cells = switch_neighbor(neighbor_name)
    ax.set_aspect(HEIGHT / WIDTH, adjustable='box')
    # grid = init(randrow=random_start)
    switch_option(option)
    create_center_of_gravity()

    mat = ax.matshow(grid, cmap=cmap, norm=matplotlib.colors.Normalize(vmin=0.0000000001, vmax=max(1, np.amax(grid))),
                     aspect='equal', interpolation='none', )  # extent=[0.25,0.75,0.25,0.75]


def offset(cells, delta):
    "Slide/offset all the cells by delta, a (dx, dy) vector."
    (dx, dy) = delta
    if bc is 'Periodyczne':
        return [((x + dx) % WIDTH, (y + dy) % HEIGHT) for (x, y) in cells]
    else:
        return [(max(0, min((x + dx), WIDTH - 1)), max(0, min((y + dy), HEIGHT - 1))) for (x, y) in cells]


# neighbor_cells = switch_neighbor(neighbor_name)

fig, ax = plt.subplots()
plt.subplots_adjust(left=0.2, bottom=0.2, right=0.8, top=0.8)
cmap = plt.cm.hsv
cmap.set_under(color='white')
cmap_energy = plt.cm.Greens
cmap_energy.set_under(color='white')
square_w = fig.get_figwidth() / WIDTH
square_h = fig.get_figheight() / HEIGHT
ax.axis('off')
ax.set_frame_on(True)
grid = None
energy_grid = None
centre_of_mass = None
centre_tree = None
# a = None
mat = None

textstr = 'Couldn\'t fit all seeds'
props = dict(boxstyle='round', facecolor='lightcoral', alpha=0.5)

textax1 = plt.axes([0.85, 0.85, 0.1, 0.04])
textbox1 = matplotlib.widgets.TextBox(textax1, 'W:', initial=str(WIDTH))

textax2 = plt.axes([0.85, 0.75, 0.1, 0.04])
textbox2 = matplotlib.widgets.TextBox(textax2, 'H:', initial=str(HEIGHT))

textax3 = plt.axes([0.85, 0.65, 0.1, 0.04])
textbox3 = matplotlib.widgets.TextBox(textax3, 'Cell\n ID:', initial=str(cell_id))

textax4 = plt.axes([0.85, 0.55, 0.1, 0.04])
textbox4 = matplotlib.widgets.TextBox(textax4, 'r:', initial=str(r))

texterr = plt.axes([0.85, 0.45, 0.1, 0.04], frame_on=False, )
texterr.set_axis_off()
texterr.text(0, 0, textstr, transform=texterr.transAxes, fontsize=6, verticalalignment='top', bbox=props, wrap=True)
texterr.set_visible(False)

textax5 = plt.axes([0.85, 0.10, 0.1, 0.04])
textbox5 = matplotlib.widgets.TextBox(textax5, 'kt:', initial=str(kt))

textax6 = plt.axes([0.85, 0.15, 0.1, 0.04])
textbox6 = matplotlib.widgets.TextBox(textax6, 'MC Iter:', initial=str(MC_count))

def set_width(event):
    global tmpWIDTH
    try:
        tmpWIDTH = int(event)
        if tmpWIDTH <= 0:
            raise Exception
    except:
        textbox1.set_val(WIDTH)
        pass


textbox1.on_submit(set_width)


def set_height(event):
    global tmpHEIGHT
    try:
        tmpHEIGHT = int(event)
        if tmpHEIGHT <= 0:
            raise Exception
    except:
        textbox2.set_val(HEIGHT)
        pass


textbox2.on_submit(set_height)


def set_cellid(event):
    global cell_id
    try:
        tmp_cellid = int(event)
        if tmp_cellid > 0:
            cell_id = tmp_cellid
        else:
            raise Exception
    except:
        textbox3.set_val(cell_id)
        pass


textbox3.on_submit(set_cellid)


def set_r(event):
    global r
    try:
        tmp_r = int(event)
        if tmp_r > 0:
            r = tmp_r
        else:
            raise Exception
    except:
        textbox4.set_val(r)
        pass


textbox4.on_submit(set_r)


def set_kt(event):
    global kt
    print(event)
    try:
        tmp_kt = float(event)
        if 0.1 <= tmp_kt <= 6:
            kt = tmp_kt
        else:
            raise Exception
    except:
        textbox5.set_val(kt)
        pass

textbox5.on_submit(set_kt)


def set_mc_count(event):
    global MC_count
    try:
        tmp_MC_count = int(event)
        if 100 <= tmp_MC_count <= 100000:
            MC_count = tmp_MC_count
        else:
            raise Exception
    except:
        textbox6.set_val(MC_count)
        pass

textbox6.on_submit(set_mc_count)


def update(d):
    print(d)
    global grid, changed, ani
    changed = False
    grid_prev = np.copy(grid)
    it = np.nditer(grid_prev, flags=['multi_index'], op_flags=['writeonly'])
    # print(grid)
    while not it.finished:
        # y1 = it.multi_index[0]
        # x1 = it.multi_index[1]
        if not it[0]:
            pairs = offset(switch_neighbor(neighbor_name), (it.multi_index[1], it.multi_index[0]))
            neighbours = [grid_prev[y][x] for (x, y) in pairs]
            if np.count_nonzero(neighbours):
                grid[it.multi_index[0]][it.multi_index[1]] = np.bincount(
                    [int(tmp) for tmp in np.setdiff1d(neighbours, [0])]).argmax()
                changed = True
            #     idx = np.flatnonzero(neighbours)
            #     grid[it.multi_index[0]][it.multi_index[1]] = grid_prev[pairs[idx[0]][1]][pairs[idx[0]][0]]
            # else:
            #     pass

        it.iternext()
    if not changed:
        ani.stop()
        ani.runs = False
    del grid_prev
    mat.set_data(grid)
    # return mat


def calculate_energy(neighbors, cell_id):
    neighbors_bin = np.bincount(neighbors)
    try:
        return len(neighbors) - neighbors_bin[int(cell_id)]
    except IndexError:
        return len(neighbors)


def update_energy(d):
    def calculate_probability(energy_delta):
        return math.exp(-energy_delta / kt)

    global grid, changed, ani, kt
    grid_idx = []
    for index, _ in np.ndenumerate(grid):
        grid_idx.append(index)
    grid_idx = np.array([(x, y) for x, y in grid_idx])
    random_indices = np.random.choice(len(grid_idx), MC_count, replace=False)
    grid_idx = grid_idx[random_indices]

    for x, y in grid_idx:
        pairs = offset(switch_neighbor(neighbor_name), (x, y))
        neighbours = [grid[y][x] for (x, y) in pairs]
        cell_val = grid[y][x]
        energy_og = calculate_energy(neighbours, cell_val)
        neighbour_val = np.random.choice(neighbours)
        energy_changed = calculate_energy(neighbours, neighbour_val)
        energy_delta = energy_changed - energy_og
        if energy_delta <= 0:
            # zmniejszona energia, wiec zamieniamy
            grid[y][x] = neighbour_val
        else:
            prob = calculate_probability(energy_delta)
            grid[y][x] = np.random.choice([neighbour_val, cell_val], p=[prob, 1 - prob])

    mat.set_data(grid)

def switch_view(event):

    global show_energy, grid, energy_grid

    show_energy = not show_energy

    if show_energy:
        energy_grid = np.zeros_like(grid)
        it = np.nditer(grid, flags=['multi_index'], op_flags=['writeonly'])
        while not it.finished:
            # y1 = it.multi_index[0]
            # x1 = it.multi_index[1]
            pairs = offset(switch_neighbor(neighbor_name), (it.multi_index[1], it.multi_index[0]))
            neighbours = [grid[y][x] for (x, y) in pairs]
            energy_grid[it.multi_index[0]][it.multi_index[1]] = calculate_energy(neighbours, grid[it.multi_index[0]][it.multi_index[1]])
            it.iternext()
        mat.set_cmap(cmap_energy)
        mat.set_norm(matplotlib.colors.Normalize(vmin=0.0000000001, vmax=max(1, np.amax(energy_grid))))
        mat.set_data(energy_grid)

        pass
    else:
        mat.set_cmap(cmap)
        mat.set_norm(matplotlib.colors.Normalize(vmin=0.0000000001, vmax=max(1, np.amax(grid))))
        mat.set_data(grid)
        pass

eps_dot = None
A = None
B = None





ani = Player(fig, update, maxi=2)

plt.show()
