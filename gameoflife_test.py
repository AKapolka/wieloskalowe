from collections import Counter


def life(world, N):
    "Play Conway's game of life for N generations from initial world."
    for g in range(N + 1):
        display(world, g)
        counts = Counter(n for c in world for n in offset(neighboring_cells, c))
        world = {c for c in counts
                 if counts[c] == 3 or (counts[c] == 2 and c in world)}


neighboring_cells = [(-1, -1), (0, -1), (1, -1),
                     (-1, 0), (1, 0),
                     (-1, 1), (0, 1), (1, 1)]

neighboring_cells_neumann = [(0, -1),
                             (-1, 0), (1, 0),
                             (0, 1)]

neighboring_cells_glider = [(1, 0), (2, 0),
                            (0, 1), (1, 1),
                            (2, 2)]

neighboring_cells_blinker = [(1, 0),
                             (1, 1),
                             (1, 2)]

neighboring_cells_beehive = [(1, 0), (2, 0),
                             (0, 1), (3, 1),
                             (1, 2), (2, 2)]


def offset(cells, delta):
    "Slide/offset all the cells by delta, a (dx, dy) vector."
    (dx, dy) = delta
    return [((x + dx) % WIDTH, (y + dy) % HEIGHT) for (x, y) in cells]


def display(world, g):
    "Display the world as a grid of characters."
    print('          GENERATION {}:'.format(g))
    Xs, Ys = zip(*world)
    Xrange = range(min(Xs), max(Xs) + 1)
    for y in range(min(Ys), max(Ys) + 1):
        print(''.join('#' if (x, y) in world else '.' for x in Xrange))


# life(world, 5)

import numpy as np

HEIGHT = 5
WIDTH = 5

grid = np.zeros(shape=(HEIGHT, WIDTH), dtype=np.int8)

fillin = offset(neighboring_cells_beehive, (1, 1))
for (x, y) in fillin:
    grid[y][x] = 1

print(grid)
# for z in range(HEIGHT):
#     grid[z] = np.random.choice((0,1), WIDTH, p=[0.5, 0.5])
grid[1][1] = 1
grid[2][1] = 1
grid[3][1] = 1
neighbors = np.zeros_like(grid)
grid_prev = np.copy(grid)
it = np.nditer(grid_prev, flags=['multi_index'], op_flags=['writeonly'])
print(grid)
while not it.finished:
    y1 = it.multi_index[0]
    x1 = it.multi_index[1]
    gg = offset(neighboring_cells, (it.multi_index[1], it.multi_index[0]))
    a = [grid_prev[y % HEIGHT][x % WIDTH] for (x, y) in gg]
    neighbors[it.multi_index[0]][it.multi_index[1]] = sum(a)
    if it[0]:
        if neighbors[it.multi_index[0]][it.multi_index[1]] < 2 or neighbors[it.multi_index[0]][it.multi_index[1]] > 3:
            grid[it.multi_index[0]][it.multi_index[1]] = 0
    else:
        if neighbors[it.multi_index[0]][it.multi_index[1]] == 3:
            grid[it.multi_index[0]][it.multi_index[1]] = 1

    it.iternext()
print(neighbors)

# a = np.arange(15).reshape(HEIGHT,WIDTH)
print('\n')
# print(a)
print(grid)
print(grid[4][0])
# print(a[4%HEIGHT][0])
